export const addSpaceBetween = (value) => value.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
export const delSpaceBetween = (value) => value.replaceAll(/\s/g, "");
export const inputValueToNumber = (value) => Number(delSpaceBetween(value));
export const inputControl = (e, min, max) => {
  let number = Number(e.target.value.replaceAll(/\s/g, ""));
  number = number > max ? max : number < min ? min : number;
  e.target.value = addSpaceBetween(number.toString());
};
export function disableInputChars(event, enabledInputRegex, excludeSpecialChars) {
    console.log(event);
    excludeSpecialChars = excludeSpecialChars || [
      "Backspace",
      "Delete",
      "Home",
      "Tab",
      "ArrowLeft",
      "ArrowRight",
      "ArrowUp",
      "ArrowDown",
      "End",
    ];
    let key = event.key;
    let keyCode = event.keyCode;
    let ctrlDown = event.ctrlKey || event.metaKey;
  
    if (ctrlDown) {
      //              c                 v                 x
      if (keyCode === 67 || keyCode === 86 || keyCode === 88) {
        return;
      }
    }
  
    if (!excludeSpecialChars.includes(key)) {
      if (!enabledInputRegex.test(key)) {
        event.preventDefault();
      }
    }
  }
export const handleChangeForSlider = (event) => {
  let target = event.target

  const min = target.min
  const max = target.max
  const val = target.value
  
  target.style.backgroundSize = (val - min) * 100 / (max - min) + '% 100%'
}

export const initSlider = (ref) => {
  let target = ref.current

  const min = target.min
  const max = target.max
  const val = target.value
  
  target.style.backgroundSize = (val - min) * 100 / (max - min) + '% 100%'
}

export const maxToMaxLenght = (value)=>value.toString().length + Math.floor((value.toString().length - 1)/3) ; 

