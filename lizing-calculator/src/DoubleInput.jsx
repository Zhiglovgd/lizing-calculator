import { useEffect, useRef } from "react";
import {
  addSpaceBetween,
  disableInputChars,
  inputControl,
  inputValueToNumber,
  handleChangeForSlider,
  initSlider,
  maxToMaxLenght,
} from "./helper";

export const DoubleInput = ({
  labelText,
  id,
  value,
  min,
  max,
  change,
  children,
  isPercent,
  active,
}) => {
  const Range = useRef(null);

  useEffect(() => {
    initSlider(Range);
  }, [value]);

  const handleBlur = (event) => {
    inputControl(event, min, max);
    change(inputValueToNumber(event.target.value));
  };

  const handleKeydown = (event) => {
    disableInputChars(event, /[0-9]/);
  };
  const handleChangeInput = (event) => {
    change(inputValueToNumber(event.target.value));
  };

  const handleChangeRange = (event) => {
    handleChangeForSlider(event);
    change(event.target.value);
  };

  return (
    <div
      className={
        active
          ? "container-button__input container-button__input_disabled"
          : "container-button__input"
      }
    >
      <label htmlFor={id} className="inputNumber-label">
        {labelText}
      </label>
      <input
        className="inputNumber"
        type="text"
        value={addSpaceBetween(value.toString())}
        id={id}
        maxLength={maxToMaxLenght(max)}
        onBlur={(e) => handleBlur(e)}
        onChange={(e) => handleChangeInput(e)}
        onKeyDown={(e) => handleKeydown(e)}
        disabled={active || isPercent}
      />
      <input
        className="inputRange"
        type="range"
        min={min}
        max={max}
        value={value}
        id={id + "-range"}
        onChange={(e) => handleChangeRange(e)}
        disabled={active}
        ref={Range}
      />
      {children}
    </div>
  );
};
