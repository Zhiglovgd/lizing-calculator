import { useState } from "react";
import { DoubleInput } from "./DoubleInput";
import { addSpaceBetween, inputValueToNumber } from "./helper";

export const Calculator = ({ priceIn, initialIn, monthIn, maxPrice, minPrice, maxMonth, minMonth, maxInitial, minInitial }) => {
  const [price, setPrice] = useState(priceIn);
  const [initial, setInitial] = useState(initialIn);
  const [month, setMonth] = useState(monthIn);
  const [isDisabled, setIsDisabled] = useState(false);
  const monthPay =
    (price - initial) *
    ((0.035 * Math.pow(1 + 0.035, month)) / (Math.pow(1 + 0.035, month) - 1));
  const amountPay = initial + month * monthPay;

  const handleChangePrice = (value) => {
    setPrice(value);
  };

  const Preloader = <img src='/images/Preloader.svg' id={"Preloader"} alt={"Загрузка..."}/>; 

  const handleClick = () => {
    setIsDisabled(true);
    const data = {
      car_coast: price,
      initail_payment: Math.round((initial * price) / 100),
      initail_payment_percent: initial,
      lease_term: month,
      total_sum: amountPay,
      monthly_payment_from: monthPay,
    };
    fetch("http://localhost:8080/https://hookb.in/eK160jgYJ6UlaRPldJ1P", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        console.log("response: " + JSON.stringify(json));
        setIsDisabled(false);
      });
  };

  const handleChangeInitial = (value) => {
    let percent = (inputValueToNumber(value) / price) * 100;

    if (percent % 1) {
      percent = Math.round(percent);
      value = addSpaceBetween(
        ((percent * price) / 100).toString()
      );
    }

    percent = percent < 10 ? 10 : percent > 60 ? 60 : percent;

    setInitial(percent);
  };

  const handleChangeMonth = (value) => {
    setMonth(value);
  };

  return (
    <>
      <h1 className="app__title">Рассчитайте стоймость автомобиля в лизинг</h1>
      <div className="container-button">
        <DoubleInput
          labelText={"Желаемая сумма кредита"}
          id={"price"}
          value={price}
          min={minPrice}
          max={maxPrice}
          change={handleChangePrice}
          isPercent={false}
          active={isDisabled}
        >
          <div className="inputNumber__symbol">&#8381;</div>
        </DoubleInput>
        <DoubleInput
          labelText={"Первый взнос"}
          id={"initial"}
          value={Math.round((price * initial) / 100)}
          min={Math.round(price * minInitial/100)}
          max={Math.round(price * maxInitial/100)}
          change={handleChangeInitial}
          isPercent={true}
          active={isDisabled}
        >
          <div
            className="inputNumber__symbol-in-block"
            id="first-payment-percent"
          >
            {initial + "%"}
          </div>
        </DoubleInput>
        <DoubleInput
          labelText={"Срок лизинга"}
          id={"month"}
          value={month}
          min={minMonth}
          max={maxMonth}
          change={handleChangeMonth}
          isPercent={false}
          active={isDisabled}
        >
          <div className="inputNumber__symbol">мес.</div>
        </DoubleInput>
      </div>
      <div className="container-result">
        <div className="cost-result">
          <span className="cost-result__title">Сумма договора лизинга</span>
          <div className="cost-result__cost">
            <span>
              {(amountPay > 0 && isFinite(amountPay) && price <= maxPrice && month <= maxMonth 
              && price >= minPrice && month >= minMonth)
                ? addSpaceBetween(Math.round(amountPay).toString())
                : "-"}
            </span>{" "}
            <span>&#8381;</span>
          </div>
        </div>
        <div className="container-result__month-cost">
          <span className="cost-result__title">Ежемесячный платеж от</span>
          <div className="cost-result__cost">
            <span>
              {(monthPay > 0 && isFinite(monthPay) && price <= maxPrice && month <= maxMonth
              && price >= minPrice && month >= minMonth)
                ? addSpaceBetween(Math.round(monthPay).toString())
                : "-"}
            </span>{" "}
            <span>&#8381;</span>
          </div>
        </div>
        <button
          className="container-result__button"
          onClick={handleClick}
          disabled={isDisabled}
        >
          {isDisabled ? Preloader : "Оставить заявку" }
        </button>
      </div>
    </>
  );
};
