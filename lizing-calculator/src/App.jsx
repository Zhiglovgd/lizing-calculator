import "./App.css";
import "./normalize.css";
import { Calculator } from "./Calculator";

function App() {
  return (
    <div className="App">
      <Calculator
        priceIn={3300000}
        initialIn={13}
        monthIn={60}
        maxPrice={6000000}
        minPrice={1000000}
        maxMonth={60}
        minMonth={1}
        maxInitial={60}
        minInitial={10}
      ></Calculator>
    </div>
  );
}

export default App;
